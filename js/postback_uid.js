/**
 * @file
 * Initiate Postback user ID.
 */

(function ($) {

  $(document).ready(function () {
    var postback_uid = getLocationParameterByName('admitad_uid');
    if (postback_uid) {
      $.cookie('admitad_postback_uid', postback_uid, {expires: 90, path: '/'});
    }
  });

  function getLocationParameterByName(name) {
    var url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
    return (results && results[2]) ? decodeURIComponent(results[2].replace(/\+/g, " ")) : "";
  }

}(jQuery));
