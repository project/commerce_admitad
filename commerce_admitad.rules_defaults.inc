<?php

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_admitad_default_rules_configuration() {
  $rules = array();

  // Add a reaction rule upon checkout completion.
  $rule = rules_reaction_rule();
  $rule->label = t('Sends postback requests to Admitad on order checkout completion.');
  $rule->tags = array('Commerce Orders');
  $rule->active = TRUE;
  $rule->event('commerce_checkout_complete');
  $rule->action('commerce_admitad_order_postback', array(
    'order:select' => 'commerce-order',
  ));
  $rule->weight = 9;
  $rules['commerce_checkout_admitad_postback_request'] = $rule;

  return $rules;
}
