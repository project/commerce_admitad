<?php

/**
 * @file
 * IM Logistic Rules hooks, actions and triggers.
 * 
 */

function commerce_admitad_rules_event_info() {
  $events = array();

  return $events;
}


function commerce_admitad_rules_action_info() {
  $actions = array();

  $actions['commerce_admitad_order_postback'] = array(
    'label' => t('Request order postback.'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
        'save' => FALSE,
      ),
    ),
    'group' => t('Commerce Order'),
    'callbacks' => array(
      'execute' => 'commerce_admitad_order_postback_rules_action',
    ),
    'access callback' => 'commerce_order_rules_access',
  );

  return $actions;
}

function commerce_admitad_order_postback_rules_action($commerce_order, $settings, RulesState $state, RulesPlugin $element) {
  $client_id = filter_input(INPUT_COOKIE, 'admitad_postback_uid');
  // Only need to invoke postback if user has our referral cookie set.
  if (!$client_id) {
    return;
  }

  if (!variable_get_value('commerce_admitad_postback_asynch')) {
    commerce_admitad_invoke_order_postback_requests($commerce_order, $client_id);
  }
  else {
    $queue = DrupalQueue::get('commerce_admitad_postback_requests_queue');
    $queue->createQueue();
    $queue->createItem(array(
      'order_id' => $commerce_order->order_id,
      'client_id' => $client_id,
    ));
  }
}