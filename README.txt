Commerce Admitad
===============

Description
-----------
Admitad affiliate network integration for Shop owners.

This module currently supports:

  - Synch or asynch order checkout postback requests.
  - ReTag scripts for all required pages.

Tokens are used to configure ReTag values for product page.

Use hook_commerce_admitad_postback_line_items_info_alter($order, &$result) to
change postback urls (you may want to change 'tariff_code' values or alter
something else).