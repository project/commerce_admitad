<?php

function commerce_admitad_variable_group_info() {
  $groups = array();

  $groups['commerce_admitad_postback_settings'] = array(
    'title' => t('Postback settings'),
    'access' => 'admitad postback settings',
  );
  $groups['commerce_admitad_retag_settings'] = array(
    'title' => t('Retag settings'),
    'access' => 'admitad retag settings',
  );
  $groups['commerce_admitad_retag_product_page_token'] = array(
    'title' => t('Retag product page tokens'),
    'access' => 'admitad retag settings',
  );

  return $groups;
}

function commerce_admitad_variable_info() {
  $variables = array();

  $variables['commerce_admitad_postback_base_url'] = array(
    'title' => t('Postback base URL'),
    'group' => 'commerce_admitad_postback_settings',
    'type' => 'url',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 32,
    ),
    'token' => TRUE,
    'default' => 'https://ad.admitad.com/r',
    'access' => 'admitad postback settings',
  );
  $variables['commerce_admitad_postback_key'] = array(
    'title' => t('Postback key'),
    'group' => 'commerce_admitad_postback_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 40,
    ),
    'token' => TRUE,
    'access' => 'admitad postback settings',
  );
  $variables['commerce_admitad_postback_campaign_code'] = array(
    'title' => t('Campaign code'),
    'group' => 'commerce_admitad_postback_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 20,
    ),
    'token' => TRUE,
    'access' => 'admitad postback settings',
  );
  $variables['commerce_admitad_postback_action_code'] = array(
    'title' => t('Default action code'),
    'group' => 'commerce_admitad_postback_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'token' => TRUE,
    'default' => 1,
    'access' => 'admitad postback settings',
  );
  $variables['commerce_admitad_postback_tariff_code'] = array(
    'title' => t('Default tariff code'),
    'group' => 'commerce_admitad_postback_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'token' => TRUE,
    'default' => 1,
    'access' => 'admitad postback settings',
  );
  $variables['commerce_admitad_postback_payment_type'] = array(
    'title' => t('Payment type'),
    'group' => 'commerce_admitad_postback_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'select',
      '#options' => array(
        'lead' => t('Lead'),
        'sale' => t('Sale'),
      ),
    ),
    'token' => TRUE,
    'default' => 'sale',
    'access' => 'admitad postback settings',
  );
  $variables['commerce_admitad_postback_asynch'] = array(
    'title' => t('Asynch mode'),
    'type' => 'boolean',
    'description' => t('When "On", task is added to queue to be invoked later rather then sending requests to Admitad server simultaniously with checkout complete.'),
    'group' => 'commerce_admitad_postback_settings',
    'element' => array(
      '#type' => 'checkbox',
    ),
    'token' => TRUE,
    'default' => TRUE,
    'access' => 'admitad postback settings',
  );

  $variables['commerce_admitad_retag_front_page_code'] = array(
    'title' => t('ReTag front page code'),
    'group' => 'commerce_admitad_retag_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'token' => TRUE,
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_category_page_code'] = array(
    'title' => t('ReTag product category page code'),
    'group' => 'commerce_admitad_retag_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'token' => TRUE,
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_category_vid'] = array(
    'title' => t('Product category taxonomy vocabulary'),
    'group' => 'commerce_admitad_retag_settings',
    'type' => 'vocabulary_vid',
    'element' => array(
      '#type' => 'select',
    ),
    'options callback' => 'commerce_admitad_get_taxonomy_vocabularies_options',
    'token' => TRUE,
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_page_code'] = array(
    'title' => t('ReTag product page code'),
    'group' => 'commerce_admitad_retag_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'token' => TRUE,
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_cart_page_code'] = array(
    'title' => t('ReTag cart page code'),
    'group' => 'commerce_admitad_retag_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'token' => TRUE,
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_checkout_complete_page_code'] = array(
    'title' => t('ReTag checkout complete page code'),
    'group' => 'commerce_admitad_retag_settings',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 10,
    ),
    'token' => TRUE,
    'access' => 'admitad retag settings',
  );

  $variables['commerce_admitad_retag_product_page_token_id'] = array(
    'title' => t('Product ID'),
    'group' => 'commerce_admitad_retag_product_page_token',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:id]',
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_page_token_category'] = array(
    'title' => t('Product category ID'),
    'group' => 'commerce_admitad_retag_product_page_token',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-type:tid]',
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_page_token_vendor'] = array(
    'title' => t('Product vendor'),
    'group' => 'commerce_admitad_retag_product_page_token',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:field-brand:name]',
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_page_token_price'] = array(
    'title' => t('Product price'),
    'group' => 'commerce_admitad_retag_product_page_token',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:commerce-price:amount-decimal]',
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_page_token_name'] = array(
    'title' => t('Product title'),
    'group' => 'commerce_admitad_retag_product_page_token',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:title]',
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_page_token_url'] = array(
    'title' => t('Product URL'),
    'group' => 'commerce_admitad_retag_product_page_token',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:url:absolute]',
    'access' => 'admitad retag settings',
  );
  $variables['commerce_admitad_retag_product_page_token_picture'] = array(
    'title' => t('Product picture'),
    'group' => 'commerce_admitad_retag_product_page_token',
    'type' => 'string',
    'element' => array(
      '#type' => 'textfield',
      '#size' => 120,
    ),
    'token' => TRUE,
    'default' => '[node:field-product:field-image]',
    'access' => 'admitad retag settings',
  );

  return $variables;
}

function commerce_admitad_get_taxonomy_vocabularies_options() {
  $vocabularies_info = taxonomy_vocabulary_get_names();

  $result = array();
  foreach ($vocabularies_info as $vocabulary) {
    $result[$vocabulary->vid] = $vocabulary->name;
  }
  
  return $result;
}